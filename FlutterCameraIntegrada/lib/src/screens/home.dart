import 'dart:io';
import 'package:flutter/material.dart';
import 'package:fluttercamera/src/screens/photo_view_screen.dart';
import 'package:image_picker/image_picker.dart';

//Context nos marca error y es porque solo se puede encontrar en un statefullwidget y no en el que esta ahora
//Este widget nos va representar la pantalla de inicio
class HomeScreen extends StatefulWidget{
  //Ahora tenemos que enlazar nuestra homeState con el widget que queremos que nos represente este.
  _HomeScreenState createState()=> _HomeScreenState();
}

//Tenemos que hacer que se convierta en un fullstatgewidget
class _HomeScreenState extends State<HomeScreen>{
  //Ahora vamos a crear un arreglo de la lista lo vamos a crear vacio porque en un principio no tendremos nada
  List<PickedFile> images = [];
  //Aqui instanciamos
  ImagePicker imagePicker = ImagePicker();

  @override
  Widget build(BuildContext context) {
    //Vamos a retornar la interfaz de nuestra aplicacion
    return Scaffold(
      appBar: AppBar(
        title: Text("Usando la cámara con Flutter"),
      ),
      //Con esto cambiamos el boton que aparezca como un + y no en el centro como antes
      body: GridView.builder(
          padding: EdgeInsets.all(10),
          //Nosotros vamos a hacer que nuestro gridview solo tenga dos columnas
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            //Separacacion entre cada fila
            mainAxisSpacing: 10,
            //Separacion entre cada columna
            crossAxisSpacing: 10,
          ),
          //Ahora ira el itemcount que es para saber cuantos elementos va a mostrar y en nuestro caso tantos como encuentre
          itemCount: images.length,
          itemBuilder: (BuildContext context, int index){
            File imageFile = File(images[index].path);
            //Esto de aqui modificaremos para utilizar el photoview que necesitamos un file para poder mostrarse
            return InkWell(
              child: Image.file(imageFile),
              onTap: (){
                Navigator.push(context,
                    MaterialPageRoute(
                        builder: (context) => PhotoViewScreen(imageFile: imageFile,
                        )));
                //Y enotnces desde aqui tenemos que pasrle el archivo file a photoview entonces vamos a photo_view_Screen
              },
            );
            //return Image.file(File(images[index].path));
          },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: _optionsDialogBox,
      ),
      //Según el chico esto esta anticuado
      /*body: Center(
        child: RaisedButton(
          child: Text("PRESIONAME"),
          //Todos los botones tiene un Onpressed pero lo vamos a dejar vacio Despues
          //de haber echo el optiondialgo lo pondrremos dentro de onpresed aqui lo que hacemos
          //es ejecutar la parte de la direccion de la memoria y no a la funcion. Si fuera la funcion añadiriamos los () al final
          onPressed: _optionsDialogBox,
        ),
      ),*/
    );
  }

  //Este metodo se puede añadir en cualquier parte
  void _openCamera() async{
    //Esta era una version anterior, ahora se realiza de otra manera instanciando la clase de imagePicker y sabemos que nos devuleve un
    //PickedFile esto quiere decir que es un metodo asincrono
    PickedFile? picture = await imagePicker.getImage(source: ImageSource.camera);
    //Con esto queremos que el modal no se quede alli es decir la seleccion del dialog y con esto hacemos retroceder una pantalla atras.
    Navigator.pop(context);
    setState(() {
      //Aqui introducimos la imagen al array de arriba o arreglo
      images.add(picture!);
    });
    /*
   var picture = ImagePicker().pickImage(
       source: ImageSource.camera,
   );*/
  }
  //Este metodo para seleccionar de la galeria
  void _openGalery() async{
    PickedFile? picture = await imagePicker.getImage(source: ImageSource.gallery);
    //Con esto queremos que el modal no se quede alli es decir la seleccion del dialog
    Navigator.pop(context);
    //Bien como no refrescaba la pantalla principal cada vez q tomabamos o mirabamos una foto hemos añadido esto de SetState
    setState(() {
      //Aqui introducimos la imagen al array de arriba o arreglo
      images.add(picture!);
    });
    /*
    var picture = ImagePicker().pickImage(
      source: ImageSource.gallery,
    );*/
  }
  //Despues de esto tenemos que ir a nuestras funciones de onTap y añadirlas

  Future<void> _optionsDialogBox(){
    //Cuando el boton se presione el de presioname tengamos 2 opciones y vamos hacer que nos aparezca
    //una pequeña pantalla modal. Y lo vamos a hacer con future porque queremos que aunque se habra
    //siga funcionando la aplicaion la vamos a hacer asincrona
    return showDialog(
        context: context,
        //Funcion anonima
        builder: (BuildContext context){
          //este widget es el encargado de dibujar en pantalla nuestro cuadro de dialogo
          return AlertDialog(
            //Single.. nos va a permitir añadir muchos elementos y que se genere automaticamente
            content: SingleChildScrollView(
              //vamos añadir ListBody que nos va a permitir añadir elementos de manera vertical
              child: ListBody(
                //Todas las listas necesitan un widget llamado childern un arrelgo de todo tipo de widget
                children: <Widget>[
                  //Añadir a cualquier que envuelva este widget la capacidad de ejecutar cierto codigo cuando el
                  //usuario haga cierta accion
                  GestureDetector(
                    //Vamos a hacer que el texto actue de accion
                    child: Text('Tomar fotografía'),
                    onTap: _openCamera,
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                  ),
                  GestureDetector(
                    //Vamos a hacer que el texto actue de accion
                    child: Text('Seleccionar de Galeria'),
                    onTap: _openGalery,
                  )
                ],
              ),
            ),
          );
        }
    );
  }
}