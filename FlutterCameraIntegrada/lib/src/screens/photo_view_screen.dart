import 'dart:io';

import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class PhotoViewScreen extends StatelessWidget{
  PhotoViewScreen({required this.imageFile});
  File imageFile;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: PhotoView(
        //Necesitamos un file image que lo tenemos en home entonces lo que haremos es ir
        imageProvider: FileImage(imageFile),
      ),
    );
  }
}