import './company.dart';

//Para guardar los datos
class Job {
  String location;
  String role;
  Company company;
  bool isFavorite;

  Job({required this.role, required this.location, required this.company, this.isFavorite = false});
}